require 'test_helper'

class LightingconsolesControllerTest < ActionController::TestCase
  setup do
    @lightingconsole = lightingconsoles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lightingconsoles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lightingconsole" do
    assert_difference('Lightingconsole.count') do
      post :create, lightingconsole: { consolemaker: @lightingconsole.consolemaker, consolemodel: @lightingconsole.consolemodel, consolequantity: @lightingconsole.consolequantity, externalscreens: @lightingconsole.externalscreens, productimage: @lightingconsole.productimage, universe: @lightingconsole.universe }
    end

    assert_redirected_to lightingconsole_path(assigns(:lightingconsole))
  end

  test "should show lightingconsole" do
    get :show, id: @lightingconsole
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lightingconsole
    assert_response :success
  end

  test "should update lightingconsole" do
    patch :update, id: @lightingconsole, lightingconsole: { consolemaker: @lightingconsole.consolemaker, consolemodel: @lightingconsole.consolemodel, consolequantity: @lightingconsole.consolequantity, externalscreens: @lightingconsole.externalscreens, productimage: @lightingconsole.productimage, universe: @lightingconsole.universe }
    assert_redirected_to lightingconsole_path(assigns(:lightingconsole))
  end

  test "should destroy lightingconsole" do
    assert_difference('Lightingconsole.count', -1) do
      delete :destroy, id: @lightingconsole
    end

    assert_redirected_to lightingconsoles_path
  end
end
