require 'test_helper'

class NewsalesControllerTest < ActionController::TestCase
  setup do
    @newsale = newsales(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:newsales)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create newsale" do
    assert_difference('Newsale.count') do
      post :create, newsale: { productdescription: @newsale.productdescription, productimage: @newsale.productimage, productname: @newsale.productname, productprice: @newsale.productprice, productquantity: @newsale.productquantity }
    end

    assert_redirected_to newsale_path(assigns(:newsale))
  end

  test "should show newsale" do
    get :show, id: @newsale
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @newsale
    assert_response :success
  end

  test "should update newsale" do
    patch :update, id: @newsale, newsale: { productdescription: @newsale.productdescription, productimage: @newsale.productimage, productname: @newsale.productname, productprice: @newsale.productprice, productquantity: @newsale.productquantity }
    assert_redirected_to newsale_path(assigns(:newsale))
  end

  test "should destroy newsale" do
    assert_difference('Newsale.count', -1) do
      delete :destroy, id: @newsale
    end

    assert_redirected_to newsales_path
  end
end
