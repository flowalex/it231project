require 'test_helper'

class LightingaccessoriesControllerTest < ActionController::TestCase
  setup do
    @lightingaccessory = lightingaccessories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lightingaccessories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lightingaccessory" do
    assert_difference('Lightingaccessory.count') do
      post :create, lightingaccessory: { accessoryquantity: @lightingaccessory.accessoryquantity, accessorytype: @lightingaccessory.accessorytype, compatablefixtures: @lightingaccessory.compatablefixtures, productimage: @lightingaccessory.productimage }
    end

    assert_redirected_to lightingaccessory_path(assigns(:lightingaccessory))
  end

  test "should show lightingaccessory" do
    get :show, id: @lightingaccessory
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lightingaccessory
    assert_response :success
  end

  test "should update lightingaccessory" do
    patch :update, id: @lightingaccessory, lightingaccessory: { accessoryquantity: @lightingaccessory.accessoryquantity, accessorytype: @lightingaccessory.accessorytype, compatablefixtures: @lightingaccessory.compatablefixtures, productimage: @lightingaccessory.productimage }
    assert_redirected_to lightingaccessory_path(assigns(:lightingaccessory))
  end

  test "should destroy lightingaccessory" do
    assert_difference('Lightingaccessory.count', -1) do
      delete :destroy, id: @lightingaccessory
    end

    assert_redirected_to lightingaccessories_path
  end
end
