require 'test_helper'

class ProductinfoControllerTest < ActionController::TestCase
  test "should get sound-equipment" do
    get :sound-equipment
    assert_response :success
  end

  test "should get light-equipment" do
    get :light-equipment
    assert_response :success
  end

end
