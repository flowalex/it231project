require 'test_helper'

class LightingProductsControllerTest < ActionController::TestCase
  setup do
    @lighting_product = lighting_products(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lighting_products)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lighting_product" do
    assert_difference('LightingProduct.count') do
      post :create, lighting_product: { maker: @lighting_product.maker, name: @lighting_product.name, price: @lighting_product.price, quantity: @lighting_product.quantity, type: @lighting_product.type }
    end

    assert_redirected_to lighting_product_path(assigns(:lighting_product))
  end

  test "should show lighting_product" do
    get :show, id: @lighting_product
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lighting_product
    assert_response :success
  end

  test "should update lighting_product" do
    patch :update, id: @lighting_product, lighting_product: { maker: @lighting_product.maker, name: @lighting_product.name, price: @lighting_product.price, quantity: @lighting_product.quantity, type: @lighting_product.type }
    assert_redirected_to lighting_product_path(assigns(:lighting_product))
  end

  test "should destroy lighting_product" do
    assert_difference('LightingProduct.count', -1) do
      delete :destroy, id: @lighting_product
    end

    assert_redirected_to lighting_products_path
  end
end
