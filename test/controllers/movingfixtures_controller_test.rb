require 'test_helper'

class MovingfixturesControllerTest < ActionController::TestCase
  setup do
    @movingfixture = movingfixtures(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:movingfixtures)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create movingfixture" do
    assert_difference('Movingfixture.count') do
      post :create, movingfixture: { fixturemaker: @movingfixture.fixturemaker, fixturemodel: @movingfixture.fixturemodel, fixturename: @movingfixture.fixturename, fixturequantity: @movingfixture.fixturequantity, fixturewattage: @movingfixture.fixturewattage, led: @movingfixture.led, productimage: @movingfixture.productimage }
    end

    assert_redirected_to movingfixture_path(assigns(:movingfixture))
  end

  test "should show movingfixture" do
    get :show, id: @movingfixture
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @movingfixture
    assert_response :success
  end

  test "should update movingfixture" do
    patch :update, id: @movingfixture, movingfixture: { fixturemaker: @movingfixture.fixturemaker, fixturemodel: @movingfixture.fixturemodel, fixturename: @movingfixture.fixturename, fixturequantity: @movingfixture.fixturequantity, fixturewattage: @movingfixture.fixturewattage, led: @movingfixture.led, productimage: @movingfixture.productimage }
    assert_redirected_to movingfixture_path(assigns(:movingfixture))
  end

  test "should destroy movingfixture" do
    assert_difference('Movingfixture.count', -1) do
      delete :destroy, id: @movingfixture
    end

    assert_redirected_to movingfixtures_path
  end
end
