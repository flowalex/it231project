require 'test_helper'

class SoundcablesControllerTest < ActionController::TestCase
  setup do
    @soundcable = soundcables(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:soundcables)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create soundcable" do
    assert_difference('Soundcable.count') do
      post :create, soundcable: { balanced: @soundcable.balanced, cablelenght: @soundcable.cablelenght, cablequantity: @soundcable.cablequantity, cabletype: @soundcable.cabletype }
    end

    assert_redirected_to soundcable_path(assigns(:soundcable))
  end

  test "should show soundcable" do
    get :show, id: @soundcable
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @soundcable
    assert_response :success
  end

  test "should update soundcable" do
    patch :update, id: @soundcable, soundcable: { balanced: @soundcable.balanced, cablelenght: @soundcable.cablelenght, cablequantity: @soundcable.cablequantity, cabletype: @soundcable.cabletype }
    assert_redirected_to soundcable_path(assigns(:soundcable))
  end

  test "should destroy soundcable" do
    assert_difference('Soundcable.count', -1) do
      delete :destroy, id: @soundcable
    end

    assert_redirected_to soundcables_path
  end
end
