require 'test_helper'

class ConventionalfixturesControllerTest < ActionController::TestCase
  setup do
    @conventionalfixture = conventionalfixtures(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:conventionalfixtures)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create conventionalfixture" do
    assert_difference('Conventionalfixture.count') do
      post :create, conventionalfixture: { fixturelampwattage: @conventionalfixture.fixturelampwattage, fixturemaker: @conventionalfixture.fixturemaker, fixturemodel: @conventionalfixture.fixturemodel, fixturename: @conventionalfixture.fixturename, fixturequantity: @conventionalfixture.fixturequantity, productimage: @conventionalfixture.productimage }
    end

    assert_redirected_to conventionalfixture_path(assigns(:conventionalfixture))
  end

  test "should show conventionalfixture" do
    get :show, id: @conventionalfixture
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @conventionalfixture
    assert_response :success
  end

  test "should update conventionalfixture" do
    patch :update, id: @conventionalfixture, conventionalfixture: { fixturelampwattage: @conventionalfixture.fixturelampwattage, fixturemaker: @conventionalfixture.fixturemaker, fixturemodel: @conventionalfixture.fixturemodel, fixturename: @conventionalfixture.fixturename, fixturequantity: @conventionalfixture.fixturequantity, productimage: @conventionalfixture.productimage }
    assert_redirected_to conventionalfixture_path(assigns(:conventionalfixture))
  end

  test "should destroy conventionalfixture" do
    assert_difference('Conventionalfixture.count', -1) do
      delete :destroy, id: @conventionalfixture
    end

    assert_redirected_to conventionalfixtures_path
  end
end
