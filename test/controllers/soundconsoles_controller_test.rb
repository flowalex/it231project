require 'test_helper'

class SoundconsolesControllerTest < ActionController::TestCase
  setup do
    @soundconsole = soundconsoles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:soundconsoles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create soundconsole" do
    assert_difference('Soundconsole.count') do
      post :create, soundconsole: { analogue: @soundconsole.analogue, brand: @soundconsole.brand, digital: @soundconsole.digital, inputchannels: @soundconsole.inputchannels, model: @soundconsole.model, outputchannels: @soundconsole.outputchannels, productimage: @soundconsole.productimage, quantity: @soundconsole.quantity }
    end

    assert_redirected_to soundconsole_path(assigns(:soundconsole))
  end

  test "should show soundconsole" do
    get :show, id: @soundconsole
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @soundconsole
    assert_response :success
  end

  test "should update soundconsole" do
    patch :update, id: @soundconsole, soundconsole: { analogue: @soundconsole.analogue, brand: @soundconsole.brand, digital: @soundconsole.digital, inputchannels: @soundconsole.inputchannels, model: @soundconsole.model, outputchannels: @soundconsole.outputchannels, productimage: @soundconsole.productimage, quantity: @soundconsole.quantity }
    assert_redirected_to soundconsole_path(assigns(:soundconsole))
  end

  test "should destroy soundconsole" do
    assert_difference('Soundconsole.count', -1) do
      delete :destroy, id: @soundconsole
    end

    assert_redirected_to soundconsoles_path
  end
end
