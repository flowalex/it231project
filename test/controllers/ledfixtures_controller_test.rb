require 'test_helper'

class LedfixturesControllerTest < ActionController::TestCase
  setup do
    @ledfixture = ledfixtures(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ledfixtures)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ledfixture" do
    assert_difference('Ledfixture.count') do
      post :create, ledfixture: { fixtureledcolorscheme: @ledfixture.fixtureledcolorscheme, fixturemaker: @ledfixture.fixturemaker, fixturemodel: @ledfixture.fixturemodel, fixturename: @ledfixture.fixturename, fixturequantity: @ledfixture.fixturequantity, productimage: @ledfixture.productimage }
    end

    assert_redirected_to ledfixture_path(assigns(:ledfixture))
  end

  test "should show ledfixture" do
    get :show, id: @ledfixture
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ledfixture
    assert_response :success
  end

  test "should update ledfixture" do
    patch :update, id: @ledfixture, ledfixture: { fixtureledcolorscheme: @ledfixture.fixtureledcolorscheme, fixturemaker: @ledfixture.fixturemaker, fixturemodel: @ledfixture.fixturemodel, fixturename: @ledfixture.fixturename, fixturequantity: @ledfixture.fixturequantity, productimage: @ledfixture.productimage }
    assert_redirected_to ledfixture_path(assigns(:ledfixture))
  end

  test "should destroy ledfixture" do
    assert_difference('Ledfixture.count', -1) do
      delete :destroy, id: @ledfixture
    end

    assert_redirected_to ledfixtures_path
  end
end
