require 'test_helper'

class SoundProductsControllerTest < ActionController::TestCase
  setup do
    @sound_product = sound_products(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sound_products)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sound_product" do
    assert_difference('SoundProduct.count') do
      post :create, sound_product: { maker: @sound_product.maker, name: @sound_product.name, price: @sound_product.price, quantity: @sound_product.quantity, type: @sound_product.type }
    end

    assert_redirected_to sound_product_path(assigns(:sound_product))
  end

  test "should show sound_product" do
    get :show, id: @sound_product
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sound_product
    assert_response :success
  end

  test "should update sound_product" do
    patch :update, id: @sound_product, sound_product: { maker: @sound_product.maker, name: @sound_product.name, price: @sound_product.price, quantity: @sound_product.quantity, type: @sound_product.type }
    assert_redirected_to sound_product_path(assigns(:sound_product))
  end

  test "should destroy sound_product" do
    assert_difference('SoundProduct.count', -1) do
      delete :destroy, id: @sound_product
    end

    assert_redirected_to sound_products_path
  end
end
