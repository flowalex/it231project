require 'test_helper'

class UsedSalesControllerTest < ActionController::TestCase
  setup do
    @used_sale = used_sales(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:used_sales)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create used_sale" do
    assert_difference('UsedSale.count') do
      post :create, used_sale: { productimage: @used_sale.productimage, productname: @used_sale.productname, productprice: @used_sale.productprice, productquantity: @used_sale.productquantity }
    end

    assert_redirected_to used_sale_path(assigns(:used_sale))
  end

  test "should show used_sale" do
    get :show, id: @used_sale
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @used_sale
    assert_response :success
  end

  test "should update used_sale" do
    patch :update, id: @used_sale, used_sale: { productimage: @used_sale.productimage, productname: @used_sale.productname, productprice: @used_sale.productprice, productquantity: @used_sale.productquantity }
    assert_redirected_to used_sale_path(assigns(:used_sale))
  end

  test "should destroy used_sale" do
    assert_difference('UsedSale.count', -1) do
      delete :destroy, id: @used_sale
    end

    assert_redirected_to used_sales_path
  end
end
