require 'test_helper'

class LightingcablesControllerTest < ActionController::TestCase
  setup do
    @lightingcable = lightingcables(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lightingcables)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lightingcable" do
    assert_difference('Lightingcable.count') do
      post :create, lightingcable: { cablelength: @lightingcable.cablelength, cablequantityinteger: @lightingcable.cablequantityinteger, cabletype: @lightingcable.cabletype, dmxpin: @lightingcable.dmxpin }
    end

    assert_redirected_to lightingcable_path(assigns(:lightingcable))
  end

  test "should show lightingcable" do
    get :show, id: @lightingcable
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lightingcable
    assert_response :success
  end

  test "should update lightingcable" do
    patch :update, id: @lightingcable, lightingcable: { cablelength: @lightingcable.cablelength, cablequantityinteger: @lightingcable.cablequantityinteger, cabletype: @lightingcable.cabletype, dmxpin: @lightingcable.dmxpin }
    assert_redirected_to lightingcable_path(assigns(:lightingcable))
  end

  test "should destroy lightingcable" do
    assert_difference('Lightingcable.count', -1) do
      delete :destroy, id: @lightingcable
    end

    assert_redirected_to lightingcables_path
  end
end
