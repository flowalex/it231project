require 'test_helper'

class WirelessmicsControllerTest < ActionController::TestCase
  setup do
    @wirelessmic = wirelessmics(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:wirelessmics)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create wirelessmic" do
    assert_difference('Wirelessmic.count') do
      post :create, wirelessmic: { productimage: @wirelessmic.productimage, quantity: @wirelessmic.quantity, type: @wirelessmic.type }
    end

    assert_redirected_to wirelessmic_path(assigns(:wirelessmic))
  end

  test "should show wirelessmic" do
    get :show, id: @wirelessmic
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @wirelessmic
    assert_response :success
  end

  test "should update wirelessmic" do
    patch :update, id: @wirelessmic, wirelessmic: { productimage: @wirelessmic.productimage, quantity: @wirelessmic.quantity, type: @wirelessmic.type }
    assert_redirected_to wirelessmic_path(assigns(:wirelessmic))
  end

  test "should destroy wirelessmic" do
    assert_difference('Wirelessmic.count', -1) do
      delete :destroy, id: @wirelessmic
    end

    assert_redirected_to wirelessmics_path
  end
end
