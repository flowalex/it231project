require 'test_helper'

class AmplifersControllerTest < ActionController::TestCase
  setup do
    @amplifer = amplifers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:amplifers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create amplifer" do
    assert_difference('Amplifer.count') do
      post :create, amplifer: { ampliferbrand: @amplifer.ampliferbrand, amplifiermodel: @amplifer.amplifiermodel, amplifierpower: @amplifer.amplifierpower, amplifierquantity: @amplifer.amplifierquantity, productimage: @amplifer.productimage }
    end

    assert_redirected_to amplifer_path(assigns(:amplifer))
  end

  test "should show amplifer" do
    get :show, id: @amplifer
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @amplifer
    assert_response :success
  end

  test "should update amplifer" do
    patch :update, id: @amplifer, amplifer: { ampliferbrand: @amplifer.ampliferbrand, amplifiermodel: @amplifer.amplifiermodel, amplifierpower: @amplifer.amplifierpower, amplifierquantity: @amplifer.amplifierquantity, productimage: @amplifer.productimage }
    assert_redirected_to amplifer_path(assigns(:amplifer))
  end

  test "should destroy amplifer" do
    assert_difference('Amplifer.count', -1) do
      delete :destroy, id: @amplifer
    end

    assert_redirected_to amplifers_path
  end
end
