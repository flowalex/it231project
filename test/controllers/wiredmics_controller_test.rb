require 'test_helper'

class WiredmicsControllerTest < ActionController::TestCase
  setup do
    @wiredmic = wiredmics(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:wiredmics)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create wiredmic" do
    assert_difference('Wiredmic.count') do
      post :create, wiredmic: { productimage: @wiredmic.productimage, quantity: @wiredmic.quantity, type: @wiredmic.type }
    end

    assert_redirected_to wiredmic_path(assigns(:wiredmic))
  end

  test "should show wiredmic" do
    get :show, id: @wiredmic
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @wiredmic
    assert_response :success
  end

  test "should update wiredmic" do
    patch :update, id: @wiredmic, wiredmic: { productimage: @wiredmic.productimage, quantity: @wiredmic.quantity, type: @wiredmic.type }
    assert_redirected_to wiredmic_path(assigns(:wiredmic))
  end

  test "should destroy wiredmic" do
    assert_difference('Wiredmic.count', -1) do
      delete :destroy, id: @wiredmic
    end

    assert_redirected_to wiredmics_path
  end
end
