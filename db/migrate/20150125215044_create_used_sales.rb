class CreateUsedSales < ActiveRecord::Migration
  def change
    create_table :used_sales do |t|
      t.string :productname
      t.float :productprice
      t.integer :productquantity
      t.string :productimage

      t.timestamps
    end
  end
end
