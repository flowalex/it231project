class CreateNewsales < ActiveRecord::Migration
  def change
    create_table :newsales do |t|
      t.string :productname
      t.float :productprice
      t.integer :productquantity
      t.string :productimage
      t.string :productdescription

      t.timestamps
    end
  end
end
