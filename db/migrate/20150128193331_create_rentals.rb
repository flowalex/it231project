class CreateRentals < ActiveRecord::Migration
  def change
    create_table :rentals do |t|
      t.string :productname
      t.float :productprice
      t.integer :productquantity
      t.string :productimage

      t.timestamps
    end
  end
end
