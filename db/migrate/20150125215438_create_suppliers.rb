class CreateSuppliers < ActiveRecord::Migration
  def change
    create_table :suppliers do |t|
      t.string :companyname
      t.string :repname
      t.string :repphone
      t.string :repemail
      t.string :repaddress
      t.string :repcity
      t.string :repstate

      t.timestamps
    end
  end
end
