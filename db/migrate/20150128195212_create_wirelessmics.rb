class CreateWirelessmics < ActiveRecord::Migration
  def change
    create_table :wirelessmics do |t|
      t.string :type
      t.integer :quantity
      t.string :productimage

      t.timestamps
    end
  end
end
