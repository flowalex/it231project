class CreateSoundconsoles < ActiveRecord::Migration
  def change
    create_table :soundconsoles do |t|
      t.string :brand
      t.string :model
      t.integer :quantity
      t.integer :inputchannels
      t.integer :outputchannels
      t.boolean :digital
      t.boolean :analogue
      t.string :productimage

      t.timestamps
    end
  end
end
