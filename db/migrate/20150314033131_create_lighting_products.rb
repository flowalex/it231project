class CreateLightingProducts < ActiveRecord::Migration
  def change
    create_table :lighting_products do |t|
      t.string :maker
      t.string :name
      t.integer :quantity
      t.string :price
      t.string :type

      t.timestamps
    end
  end
end
