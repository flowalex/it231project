class CreateLightingconsoles < ActiveRecord::Migration
  def change
    create_table :lightingconsoles do |t|
      t.string :consolemaker
      t.string :consolemodel
      t.integer :universe
      t.boolean :externalscreens
      t.integer :consolequantity
      t.string :productimage

      t.timestamps
    end
  end
end
