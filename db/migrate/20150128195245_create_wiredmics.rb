class CreateWiredmics < ActiveRecord::Migration
  def change
    create_table :wiredmics do |t|
      t.string :type
      t.integer :quantity
      t.string :productimage

      t.timestamps
    end
  end
end
