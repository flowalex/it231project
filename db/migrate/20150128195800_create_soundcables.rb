class CreateSoundcables < ActiveRecord::Migration
  def change
    create_table :soundcables do |t|
      t.string :cabletype
      t.integer :cablelenght
      t.integer :cablequantity
      t.boolean :balanced

      t.timestamps
    end
  end
end
