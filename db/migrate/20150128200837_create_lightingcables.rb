class CreateLightingcables < ActiveRecord::Migration
  def change
    create_table :lightingcables do |t|
      t.string :cabletype
      t.string :dmxpin
      t.string :cablequantityinteger
      t.integer :cablelength

      t.timestamps
    end
  end
end
