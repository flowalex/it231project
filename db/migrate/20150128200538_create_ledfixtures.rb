class CreateLedfixtures < ActiveRecord::Migration
  def change
    create_table :ledfixtures do |t|
      t.string :fixturemaker
      t.string :fixturemodel
      t.string :fixturename
      t.string :fixtureledcolorscheme
      t.integer :fixturequantity
      t.string :productimage

      t.timestamps
    end
  end
end
