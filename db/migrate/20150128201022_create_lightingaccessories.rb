class CreateLightingaccessories < ActiveRecord::Migration
  def change
    create_table :lightingaccessories do |t|
      t.string :accessorytype
      t.text :compatablefixtures
      t.integer :accessoryquantity
      t.string :productimage

      t.timestamps
    end
  end
end
