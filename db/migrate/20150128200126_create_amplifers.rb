class CreateAmplifers < ActiveRecord::Migration
  def change
    create_table :amplifers do |t|
      t.string :ampliferbrand
      t.string :amplifiermodel
      t.string :amplifierpower
      t.integer :amplifierquantity
      t.string :productimage

      t.timestamps
    end
  end
end
