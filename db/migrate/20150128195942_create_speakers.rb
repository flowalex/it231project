class CreateSpeakers < ActiveRecord::Migration
  def change
    create_table :speakers do |t|
      t.string :speakerbrand
      t.string :speakermodel
      t.string :speakertype
      t.integer :speakerquantity
      t.string :productimage

      t.timestamps
    end
  end
end
