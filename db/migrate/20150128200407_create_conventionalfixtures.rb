class CreateConventionalfixtures < ActiveRecord::Migration
  def change
    create_table :conventionalfixtures do |t|
      t.string :fixturemaker
      t.string :fixturemodel
      t.string :fixturename
      t.string :fixturelampwattage
      t.integer :fixturequantity
      t.string :productimage

      t.timestamps
    end
  end
end
