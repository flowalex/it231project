class CreateMovingfixtures < ActiveRecord::Migration
  def change
    create_table :movingfixtures do |t|
      t.string :fixturemaker
      t.string :fixturemodel
      t.string :fixturename
      t.boolean :led
      t.string :fixturewattage
      t.integer :fixturequantity
      t.string :productimage

      t.timestamps
    end
  end
end
