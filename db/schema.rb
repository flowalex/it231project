# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150314033443) do

  create_table "amplifers", force: true do |t|
    t.string   "ampliferbrand"
    t.string   "amplifiermodel"
    t.string   "amplifierpower"
    t.integer  "amplifierquantity"
    t.string   "productimage"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "conventionalfixtures", force: true do |t|
    t.string   "fixturemaker"
    t.string   "fixturemodel"
    t.string   "fixturename"
    t.string   "fixturelampwattage"
    t.integer  "fixturequantity"
    t.string   "productimage"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "customers", force: true do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "email"
    t.string   "phone"
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.text     "addionalnotes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "username"
    t.string   "password_digest"
  end

  create_table "employees", force: true do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "employeeemail"
    t.string   "employeephone"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ledfixtures", force: true do |t|
    t.string   "fixturemaker"
    t.string   "fixturemodel"
    t.string   "fixturename"
    t.string   "fixtureledcolorscheme"
    t.integer  "fixturequantity"
    t.string   "productimage"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lighting_products", force: true do |t|
    t.string   "maker"
    t.string   "name"
    t.integer  "quantity"
    t.string   "price"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lightingaccessories", force: true do |t|
    t.string   "accessorytype"
    t.text     "compatablefixtures"
    t.integer  "accessoryquantity"
    t.string   "productimage"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lightingcables", force: true do |t|
    t.string   "cabletype"
    t.string   "dmxpin"
    t.string   "cablequantityinteger"
    t.integer  "cablelength"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lightingconsoles", force: true do |t|
    t.string   "consolemaker"
    t.string   "consolemodel"
    t.integer  "universe"
    t.boolean  "externalscreens"
    t.integer  "consolequantity"
    t.string   "productimage"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "movingfixtures", force: true do |t|
    t.string   "fixturemaker"
    t.string   "fixturemodel"
    t.string   "fixturename"
    t.boolean  "led"
    t.string   "fixturewattage"
    t.integer  "fixturequantity"
    t.string   "productimage"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "newsales", force: true do |t|
    t.string   "productname"
    t.float    "productprice"
    t.integer  "productquantity"
    t.string   "productimage"
    t.string   "productdescription"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "promotions", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.string   "imagepath"
    t.date     "startdate"
    t.date     "enddate"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rentals", force: true do |t|
    t.string   "productname"
    t.float    "productprice"
    t.integer  "productquantity"
    t.string   "productimage"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sound_products", force: true do |t|
    t.string   "maker"
    t.string   "name"
    t.integer  "quantity"
    t.string   "price"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "soundcables", force: true do |t|
    t.string   "cabletype"
    t.integer  "cablelenght"
    t.integer  "cablequantity"
    t.boolean  "balanced"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "soundconsoles", force: true do |t|
    t.string   "brand"
    t.string   "model"
    t.integer  "quantity"
    t.integer  "inputchannels"
    t.integer  "outputchannels"
    t.boolean  "digital"
    t.boolean  "analogue"
    t.string   "productimage"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "speakers", force: true do |t|
    t.string   "speakerbrand"
    t.string   "speakermodel"
    t.string   "speakertype"
    t.integer  "speakerquantity"
    t.string   "productimage"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "suppliers", force: true do |t|
    t.string   "companyname"
    t.string   "repname"
    t.string   "repphone"
    t.string   "repemail"
    t.string   "repaddress"
    t.string   "repcity"
    t.string   "repstate"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "used_sales", force: true do |t|
    t.string   "productname"
    t.float    "productprice"
    t.integer  "productquantity"
    t.string   "productimage"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "wiredmics", force: true do |t|
    t.string   "type"
    t.integer  "quantity"
    t.string   "productimage"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "wirelessmics", force: true do |t|
    t.string   "type"
    t.integer  "quantity"
    t.string   "productimage"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
