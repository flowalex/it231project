class ConventionalfixturesController < ApplicationController
  before_action :set_conventionalfixture, only: [:show, :edit, :update, :destroy]

  # GET /conventionalfixtures
  # GET /conventionalfixtures.json
  def index
    @conventionalfixtures = Conventionalfixture.all
  end

  # GET /conventionalfixtures/1
  # GET /conventionalfixtures/1.json
  def show
  end

  # GET /conventionalfixtures/new
  def new
    @conventionalfixture = Conventionalfixture.new
  end

  # GET /conventionalfixtures/1/edit
  def edit
  end

  # POST /conventionalfixtures
  # POST /conventionalfixtures.json
  def create
    @conventionalfixture = Conventionalfixture.new(conventionalfixture_params)

    respond_to do |format|
      if @conventionalfixture.save
        format.html { redirect_to @conventionalfixture, notice: 'Conventionalfixture was successfully created.' }
        format.json { render :show, status: :created, location: @conventionalfixture }
      else
        format.html { render :new }
        format.json { render json: @conventionalfixture.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /conventionalfixtures/1
  # PATCH/PUT /conventionalfixtures/1.json
  def update
    respond_to do |format|
      if @conventionalfixture.update(conventionalfixture_params)
        format.html { redirect_to @conventionalfixture, notice: 'Conventionalfixture was successfully updated.' }
        format.json { render :show, status: :ok, location: @conventionalfixture }
      else
        format.html { render :edit }
        format.json { render json: @conventionalfixture.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /conventionalfixtures/1
  # DELETE /conventionalfixtures/1.json
  def destroy
    @conventionalfixture.destroy
    respond_to do |format|
      format.html { redirect_to conventionalfixtures_url, notice: 'Conventionalfixture was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_conventionalfixture
      @conventionalfixture = Conventionalfixture.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def conventionalfixture_params
      params.require(:conventionalfixture).permit(:fixturemaker, :fixturemodel, :fixturename, :fixturelampwattage, :fixturequantity, :productimage)
    end
end
