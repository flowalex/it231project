class UsedSalesController < ApplicationController
  before_action :set_used_sale, only: [:show, :edit, :update, :destroy]

  # GET /used_sales
  # GET /used_sales.json
  def index
    @used_sales = UsedSale.all
  end

  # GET /used_sales/1
  # GET /used_sales/1.json
  def show
  end

  # GET /used_sales/new
  def new
    @used_sale = UsedSale.new
  end

  # GET /used_sales/1/edit
  def edit
  end

  # POST /used_sales
  # POST /used_sales.json
  def create
    @used_sale = UsedSale.new(used_sale_params)

    respond_to do |format|
      if @used_sale.save
        format.html { redirect_to @used_sale, notice: 'Used sale was successfully created.' }
        format.json { render :show, status: :created, location: @used_sale }
      else
        format.html { render :new }
        format.json { render json: @used_sale.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /used_sales/1
  # PATCH/PUT /used_sales/1.json
  def update
    respond_to do |format|
      if @used_sale.update(used_sale_params)
        format.html { redirect_to @used_sale, notice: 'Used sale was successfully updated.' }
        format.json { render :show, status: :ok, location: @used_sale }
      else
        format.html { render :edit }
        format.json { render json: @used_sale.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /used_sales/1
  # DELETE /used_sales/1.json
  def destroy
    @used_sale.destroy
    respond_to do |format|
      format.html { redirect_to used_sales_url, notice: 'Used sale was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_used_sale
      @used_sale = UsedSale.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def used_sale_params
      params.require(:used_sale).permit(:productname, :productprice, :productquantity, :productimage)
    end
end
