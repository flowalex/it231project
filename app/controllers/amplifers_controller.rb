class AmplifersController < ApplicationController
  before_action :set_amplifer, only: [:show, :edit, :update, :destroy]

  # GET /amplifers
  # GET /amplifers.json
  def index
    @amplifers = Amplifer.all
  end

  # GET /amplifers/1
  # GET /amplifers/1.json
  def show
  end

  # GET /amplifers/new
  def new
    @amplifer = Amplifer.new
  end

  # GET /amplifers/1/edit
  def edit
  end

  # POST /amplifers
  # POST /amplifers.json
  def create
    @amplifer = Amplifer.new(amplifer_params)

    respond_to do |format|
      if @amplifer.save
        format.html { redirect_to @amplifer, notice: 'Amplifer was successfully created.' }
        format.json { render :show, status: :created, location: @amplifer }
      else
        format.html { render :new }
        format.json { render json: @amplifer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /amplifers/1
  # PATCH/PUT /amplifers/1.json
  def update
    respond_to do |format|
      if @amplifer.update(amplifer_params)
        format.html { redirect_to @amplifer, notice: 'Amplifer was successfully updated.' }
        format.json { render :show, status: :ok, location: @amplifer }
      else
        format.html { render :edit }
        format.json { render json: @amplifer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /amplifers/1
  # DELETE /amplifers/1.json
  def destroy
    @amplifer.destroy
    respond_to do |format|
      format.html { redirect_to amplifers_url, notice: 'Amplifer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_amplifer
      @amplifer = Amplifer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def amplifer_params
      params.require(:amplifer).permit(:ampliferbrand, :amplifiermodel, :amplifierpower, :amplifierquantity, :productimage)
    end
end
