class SoundcablesController < ApplicationController
  before_action :set_soundcable, only: [:show, :edit, :update, :destroy]

  # GET /soundcables
  # GET /soundcables.json
  def index
    @soundcables = Soundcable.all
  end

  # GET /soundcables/1
  # GET /soundcables/1.json
  def show
  end

  # GET /soundcables/new
  def new
    @soundcable = Soundcable.new
  end

  # GET /soundcables/1/edit
  def edit
  end

  # POST /soundcables
  # POST /soundcables.json
  def create
    @soundcable = Soundcable.new(soundcable_params)

    respond_to do |format|
      if @soundcable.save
        format.html { redirect_to @soundcable, notice: 'Soundcable was successfully created.' }
        format.json { render :show, status: :created, location: @soundcable }
      else
        format.html { render :new }
        format.json { render json: @soundcable.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /soundcables/1
  # PATCH/PUT /soundcables/1.json
  def update
    respond_to do |format|
      if @soundcable.update(soundcable_params)
        format.html { redirect_to @soundcable, notice: 'Soundcable was successfully updated.' }
        format.json { render :show, status: :ok, location: @soundcable }
      else
        format.html { render :edit }
        format.json { render json: @soundcable.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /soundcables/1
  # DELETE /soundcables/1.json
  def destroy
    @soundcable.destroy
    respond_to do |format|
      format.html { redirect_to soundcables_url, notice: 'Soundcable was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_soundcable
      @soundcable = Soundcable.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def soundcable_params
      params.require(:soundcable).permit(:cabletype, :cablelenght, :cablequantity, :balanced)
    end
end
