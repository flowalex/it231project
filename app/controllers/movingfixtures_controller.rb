class MovingfixturesController < ApplicationController
  before_action :set_movingfixture, only: [:show, :edit, :update, :destroy]

  # GET /movingfixtures
  # GET /movingfixtures.json
  def index
    @movingfixtures = Movingfixture.all
  end

  # GET /movingfixtures/1
  # GET /movingfixtures/1.json
  def show
  end

  # GET /movingfixtures/new
  def new
    @movingfixture = Movingfixture.new
  end

  # GET /movingfixtures/1/edit
  def edit
  end

  # POST /movingfixtures
  # POST /movingfixtures.json
  def create
    @movingfixture = Movingfixture.new(movingfixture_params)

    respond_to do |format|
      if @movingfixture.save
        format.html { redirect_to @movingfixture, notice: 'Movingfixture was successfully created.' }
        format.json { render :show, status: :created, location: @movingfixture }
      else
        format.html { render :new }
        format.json { render json: @movingfixture.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /movingfixtures/1
  # PATCH/PUT /movingfixtures/1.json
  def update
    respond_to do |format|
      if @movingfixture.update(movingfixture_params)
        format.html { redirect_to @movingfixture, notice: 'Movingfixture was successfully updated.' }
        format.json { render :show, status: :ok, location: @movingfixture }
      else
        format.html { render :edit }
        format.json { render json: @movingfixture.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /movingfixtures/1
  # DELETE /movingfixtures/1.json
  def destroy
    @movingfixture.destroy
    respond_to do |format|
      format.html { redirect_to movingfixtures_url, notice: 'Movingfixture was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_movingfixture
      @movingfixture = Movingfixture.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def movingfixture_params
      params.require(:movingfixture).permit(:fixturemaker, :fixturemodel, :fixturename, :led, :fixturewattage, :fixturequantity, :productimage)
    end
end
