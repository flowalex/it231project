class NewsalesController < ApplicationController
  before_action :set_newsale, only: [:show, :edit, :update, :destroy]

  # GET /newsales
  # GET /newsales.json
  def index
    @newsales = Newsale.all
  end

  # GET /newsales/1
  # GET /newsales/1.json
  def show
  end

  # GET /newsales/new
  def new
    @newsale = Newsale.new
  end

  # GET /newsales/1/edit
  def edit
      @newsale.LightingProduct_id params:"LightingProduct_id" 
      @newsale.LightingProduct_id=session[:user_id] #Person ID obtained from session ID - person's ID who's logged-in
      @newsale.productdate = Time.now # Today's date
      @newsale.productprice = LightingProduct.find_by_id(params["LightingProduct_id"]).cost #Getting cost from Product's table
      @newsale.SoundProduct_id params:"SoundProduct_id"  
      @newsale.SoundProduct_id=session[:user_id] #Person ID obtained from session ID - person's ID who's logged-in 
      @newsale.productdate = Time.now # Today's date 
      @newsale.productprice = SoundProduct.find_by_id(params["SoundProduct_id"]).cost #Getting cost from Product's table
  end

  # POST /newsales
  # POST /newsales.json
  def create
    @newsale = Newsale.new(newsale_params)

    respond_to do |format|
      if @newsale.save
        format.html { redirect_to @newsale, notice: 'Newsale was successfully created.' }
        format.json { render :show, status: :created, location: @newsale }
      else
        format.html { render :new }
        format.json { render json: @newsale.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /newsales/1
  # PATCH/PUT /newsales/1.json
  def update
    respond_to do |format|
      if @newsale.update(newsale_params)
        format.html { redirect_to @newsale, notice: 'Newsale was successfully updated.' }
        format.json { render :show, status: :ok, location: @newsale }
      else
        format.html { render :edit }
        format.json { render json: @newsale.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /newsales/1
  # DELETE /newsales/1.json
  def destroy
    @newsale.destroy
    respond_to do |format|
      format.html { redirect_to newsales_url, notice: 'Newsale was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_newsale
      @newsale = Newsale.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def newsale_params
      params.require(:newsale).permit(:productname, :productprice, :productquantity, :productimage, :productdescription)
    end
end
