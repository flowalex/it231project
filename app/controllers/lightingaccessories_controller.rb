class LightingaccessoriesController < ApplicationController
  before_action :set_lightingaccessory, only: [:show, :edit, :update, :destroy]

  # GET /lightingaccessories
  # GET /lightingaccessories.json
  def index
    @lightingaccessories = Lightingaccessory.all
  end

  # GET /lightingaccessories/1
  # GET /lightingaccessories/1.json
  def show
  end

  # GET /lightingaccessories/new
  def new
    @lightingaccessory = Lightingaccessory.new
  end

  # GET /lightingaccessories/1/edit
  def edit
  end

  # POST /lightingaccessories
  # POST /lightingaccessories.json
  def create
    @lightingaccessory = Lightingaccessory.new(lightingaccessory_params)

    respond_to do |format|
      if @lightingaccessory.save
        format.html { redirect_to @lightingaccessory, notice: 'Lightingaccessory was successfully created.' }
        format.json { render :show, status: :created, location: @lightingaccessory }
      else
        format.html { render :new }
        format.json { render json: @lightingaccessory.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lightingaccessories/1
  # PATCH/PUT /lightingaccessories/1.json
  def update
    respond_to do |format|
      if @lightingaccessory.update(lightingaccessory_params)
        format.html { redirect_to @lightingaccessory, notice: 'Lightingaccessory was successfully updated.' }
        format.json { render :show, status: :ok, location: @lightingaccessory }
      else
        format.html { render :edit }
        format.json { render json: @lightingaccessory.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lightingaccessories/1
  # DELETE /lightingaccessories/1.json
  def destroy
    @lightingaccessory.destroy
    respond_to do |format|
      format.html { redirect_to lightingaccessories_url, notice: 'Lightingaccessory was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lightingaccessory
      @lightingaccessory = Lightingaccessory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lightingaccessory_params
      params.require(:lightingaccessory).permit(:accessorytype, :compatablefixtures, :accessoryquantity, :productimage)
    end
end
