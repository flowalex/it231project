class LightingconsolesController < ApplicationController
  before_action :set_lightingconsole, only: [:show, :edit, :update, :destroy]

  # GET /lightingconsoles
  # GET /lightingconsoles.json
  def index
    @lightingconsoles = Lightingconsole.all
  end

  # GET /lightingconsoles/1
  # GET /lightingconsoles/1.json
  def show
  end

  # GET /lightingconsoles/new
  def new
    @lightingconsole = Lightingconsole.new
  end

  # GET /lightingconsoles/1/edit
  def edit
  end

  # POST /lightingconsoles
  # POST /lightingconsoles.json
  def create
    @lightingconsole = Lightingconsole.new(lightingconsole_params)

    respond_to do |format|
      if @lightingconsole.save
        format.html { redirect_to @lightingconsole, notice: 'Lightingconsole was successfully created.' }
        format.json { render :show, status: :created, location: @lightingconsole }
      else
        format.html { render :new }
        format.json { render json: @lightingconsole.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lightingconsoles/1
  # PATCH/PUT /lightingconsoles/1.json
  def update
    respond_to do |format|
      if @lightingconsole.update(lightingconsole_params)
        format.html { redirect_to @lightingconsole, notice: 'Lightingconsole was successfully updated.' }
        format.json { render :show, status: :ok, location: @lightingconsole }
      else
        format.html { render :edit }
        format.json { render json: @lightingconsole.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lightingconsoles/1
  # DELETE /lightingconsoles/1.json
  def destroy
    @lightingconsole.destroy
    respond_to do |format|
      format.html { redirect_to lightingconsoles_url, notice: 'Lightingconsole was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lightingconsole
      @lightingconsole = Lightingconsole.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lightingconsole_params
      params.require(:lightingconsole).permit(:consolemaker, :consolemodel, :universe, :externalscreens, :consolequantity, :productimage)
    end
end
