class LightingcablesController < ApplicationController
  before_action :set_lightingcable, only: [:show, :edit, :update, :destroy]

  # GET /lightingcables
  # GET /lightingcables.json
  def index
    @lightingcables = Lightingcable.all
  end

  # GET /lightingcables/1
  # GET /lightingcables/1.json
  def show
  end

  # GET /lightingcables/new
  def new
    @lightingcable = Lightingcable.new
  end

  # GET /lightingcables/1/edit
  def edit
  end

  # POST /lightingcables
  # POST /lightingcables.json
  def create
    @lightingcable = Lightingcable.new(lightingcable_params)

    respond_to do |format|
      if @lightingcable.save
        format.html { redirect_to @lightingcable, notice: 'Lightingcable was successfully created.' }
        format.json { render :show, status: :created, location: @lightingcable }
      else
        format.html { render :new }
        format.json { render json: @lightingcable.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lightingcables/1
  # PATCH/PUT /lightingcables/1.json
  def update
    respond_to do |format|
      if @lightingcable.update(lightingcable_params)
        format.html { redirect_to @lightingcable, notice: 'Lightingcable was successfully updated.' }
        format.json { render :show, status: :ok, location: @lightingcable }
      else
        format.html { render :edit }
        format.json { render json: @lightingcable.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lightingcables/1
  # DELETE /lightingcables/1.json
  def destroy
    @lightingcable.destroy
    respond_to do |format|
      format.html { redirect_to lightingcables_url, notice: 'Lightingcable was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lightingcable
      @lightingcable = Lightingcable.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lightingcable_params
      params.require(:lightingcable).permit(:cabletype, :dmxpin, :cablequantityinteger, :cablelength)
    end
end
