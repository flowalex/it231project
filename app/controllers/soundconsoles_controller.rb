class SoundconsolesController < ApplicationController
  before_action :set_soundconsole, only: [:show, :edit, :update, :destroy]

  # GET /soundconsoles
  # GET /soundconsoles.json
  def index
    @soundconsoles = Soundconsole.all
  end

  # GET /soundconsoles/1
  # GET /soundconsoles/1.json
  def show
  end

  # GET /soundconsoles/new
  def new
    @soundconsole = Soundconsole.new
  end

  # GET /soundconsoles/1/edit
  def edit
  end

  # POST /soundconsoles
  # POST /soundconsoles.json
  def create
    @soundconsole = Soundconsole.new(soundconsole_params)

    respond_to do |format|
      if @soundconsole.save
        format.html { redirect_to @soundconsole, notice: 'Soundconsole was successfully created.' }
        format.json { render :show, status: :created, location: @soundconsole }
      else
        format.html { render :new }
        format.json { render json: @soundconsole.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /soundconsoles/1
  # PATCH/PUT /soundconsoles/1.json
  def update
    respond_to do |format|
      if @soundconsole.update(soundconsole_params)
        format.html { redirect_to @soundconsole, notice: 'Soundconsole was successfully updated.' }
        format.json { render :show, status: :ok, location: @soundconsole }
      else
        format.html { render :edit }
        format.json { render json: @soundconsole.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /soundconsoles/1
  # DELETE /soundconsoles/1.json
  def destroy
    @soundconsole.destroy
    respond_to do |format|
      format.html { redirect_to soundconsoles_url, notice: 'Soundconsole was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_soundconsole
      @soundconsole = Soundconsole.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def soundconsole_params
      params.require(:soundconsole).permit(:brand, :model, :quantity, :inputchannels, :outputchannels, :digital, :analogue, :productimage)
    end
end
