class WirelessmicsController < ApplicationController
  before_action :set_wirelessmic, only: [:show, :edit, :update, :destroy]

  # GET /wirelessmics
  # GET /wirelessmics.json
  def index
    @wirelessmics = Wirelessmic.all
  end

  # GET /wirelessmics/1
  # GET /wirelessmics/1.json
  def show
  end

  # GET /wirelessmics/new
  def new
    @wirelessmic = Wirelessmic.new
  end

  # GET /wirelessmics/1/edit
  def edit
  end

  # POST /wirelessmics
  # POST /wirelessmics.json
  def create
    @wirelessmic = Wirelessmic.new(wirelessmic_params)

    respond_to do |format|
      if @wirelessmic.save
        format.html { redirect_to @wirelessmic, notice: 'Wirelessmic was successfully created.' }
        format.json { render :show, status: :created, location: @wirelessmic }
      else
        format.html { render :new }
        format.json { render json: @wirelessmic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /wirelessmics/1
  # PATCH/PUT /wirelessmics/1.json
  def update
    respond_to do |format|
      if @wirelessmic.update(wirelessmic_params)
        format.html { redirect_to @wirelessmic, notice: 'Wirelessmic was successfully updated.' }
        format.json { render :show, status: :ok, location: @wirelessmic }
      else
        format.html { render :edit }
        format.json { render json: @wirelessmic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /wirelessmics/1
  # DELETE /wirelessmics/1.json
  def destroy
    @wirelessmic.destroy
    respond_to do |format|
      format.html { redirect_to wirelessmics_url, notice: 'Wirelessmic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_wirelessmic
      @wirelessmic = Wirelessmic.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def wirelessmic_params
      params.require(:wirelessmic).permit(:type, :quantity, :productimage)
    end
end
