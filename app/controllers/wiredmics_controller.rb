class WiredmicsController < ApplicationController
  before_action :set_wiredmic, only: [:show, :edit, :update, :destroy]

  # GET /wiredmics
  # GET /wiredmics.json
  def index
    @wiredmics = Wiredmic.all
  end

  # GET /wiredmics/1
  # GET /wiredmics/1.json
  def show
  end

  # GET /wiredmics/new
  def new
    @wiredmic = Wiredmic.new
  end

  # GET /wiredmics/1/edit
  def edit
  end

  # POST /wiredmics
  # POST /wiredmics.json
  def create
    @wiredmic = Wiredmic.new(wiredmic_params)

    respond_to do |format|
      if @wiredmic.save
        format.html { redirect_to @wiredmic, notice: 'Wiredmic was successfully created.' }
        format.json { render :show, status: :created, location: @wiredmic }
      else
        format.html { render :new }
        format.json { render json: @wiredmic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /wiredmics/1
  # PATCH/PUT /wiredmics/1.json
  def update
    respond_to do |format|
      if @wiredmic.update(wiredmic_params)
        format.html { redirect_to @wiredmic, notice: 'Wiredmic was successfully updated.' }
        format.json { render :show, status: :ok, location: @wiredmic }
      else
        format.html { render :edit }
        format.json { render json: @wiredmic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /wiredmics/1
  # DELETE /wiredmics/1.json
  def destroy
    @wiredmic.destroy
    respond_to do |format|
      format.html { redirect_to wiredmics_url, notice: 'Wiredmic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_wiredmic
      @wiredmic = Wiredmic.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def wiredmic_params
      params.require(:wiredmic).permit(:type, :quantity, :productimage)
    end
end
