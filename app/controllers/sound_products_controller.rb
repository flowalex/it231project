class SoundProductsController < ApplicationController
  before_action :set_sound_product, only: [:show, :edit, :update, :destroy]

  # GET /sound_products
  # GET /sound_products.json
  def index
    @sound_products = SoundProduct.all
  end

  # GET /sound_products/1
  # GET /sound_products/1.json
  def show
  end

  # GET /sound_products/new
  def new
    @sound_product = SoundProduct.new
  end

  # GET /sound_products/1/edit
  def edit
  end

  # POST /sound_products
  # POST /sound_products.json
  def create
    @sound_product = SoundProduct.new(sound_product_params)

    respond_to do |format|
      if @sound_product.save
        format.html { redirect_to @sound_product, notice: 'Sound product was successfully created.' }
        format.json { render :show, status: :created, location: @sound_product }
      else
        format.html { render :new }
        format.json { render json: @sound_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sound_products/1
  # PATCH/PUT /sound_products/1.json
  def update
    respond_to do |format|
      if @sound_product.update(sound_product_params)
        format.html { redirect_to @sound_product, notice: 'Sound product was successfully updated.' }
        format.json { render :show, status: :ok, location: @sound_product }
      else
        format.html { render :edit }
        format.json { render json: @sound_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sound_products/1
  # DELETE /sound_products/1.json
  def destroy
    @sound_product.destroy
    respond_to do |format|
      format.html { redirect_to sound_products_url, notice: 'Sound product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sound_product
      @sound_product = SoundProduct.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sound_product_params
      params.require(:sound_product).permit(:maker, :name, :quantity, :price, :type)
    end
end
