class LedfixturesController < ApplicationController
  before_action :set_ledfixture, only: [:show, :edit, :update, :destroy]

  # GET /ledfixtures
  # GET /ledfixtures.json
  def index
    @ledfixtures = Ledfixture.all
  end

  # GET /ledfixtures/1
  # GET /ledfixtures/1.json
  def show
  end

  # GET /ledfixtures/new
  def new
    @ledfixture = Ledfixture.new
  end

  # GET /ledfixtures/1/edit
  def edit
  end

  # POST /ledfixtures
  # POST /ledfixtures.json
  def create
    @ledfixture = Ledfixture.new(ledfixture_params)

    respond_to do |format|
      if @ledfixture.save
        format.html { redirect_to @ledfixture, notice: 'Ledfixture was successfully created.' }
        format.json { render :show, status: :created, location: @ledfixture }
      else
        format.html { render :new }
        format.json { render json: @ledfixture.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ledfixtures/1
  # PATCH/PUT /ledfixtures/1.json
  def update
    respond_to do |format|
      if @ledfixture.update(ledfixture_params)
        format.html { redirect_to @ledfixture, notice: 'Ledfixture was successfully updated.' }
        format.json { render :show, status: :ok, location: @ledfixture }
      else
        format.html { render :edit }
        format.json { render json: @ledfixture.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ledfixtures/1
  # DELETE /ledfixtures/1.json
  def destroy
    @ledfixture.destroy
    respond_to do |format|
      format.html { redirect_to ledfixtures_url, notice: 'Ledfixture was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ledfixture
      @ledfixture = Ledfixture.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ledfixture_params
      params.require(:ledfixture).permit(:fixturemaker, :fixturemodel, :fixturename, :fixtureledcolorscheme, :fixturequantity, :productimage)
    end
end
