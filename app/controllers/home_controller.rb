class HomeController < ApplicationController
 skip_before_filter :authorize

    def index
    @time = Time.now
  end

  def about
    
  end

  def contact
      
  end
    

  def sales
    
  end

  def rentals
    
  end
    
    def help
    
  end
    
    def privacy
    
  end
    def weekend
        @day =Time.now
        @to_saturday = 6- @day.wday
    end
    def search
       @results=0
	if !params[:searchinput].nil?
		@results=1
		@searchcriteria="%#{params[:Input]}%"	
		@productlist = Conventionalfixture.where("fixturename like ?",@searchcriteria)	
    end  
    end
    def announcements
        @promotions = Promotion.where("startdate <= ? AND enddate >= ?",  Date.today, Date.today)
    end
end
