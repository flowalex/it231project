json.array!(@soundconsoles) do |soundconsole|
  json.extract! soundconsole, :id, :brand, :model, :quantity, :inputchannels, :outputchannels, :digital, :analogue, :productimage
  json.url soundconsole_url(soundconsole, format: :json)
end
