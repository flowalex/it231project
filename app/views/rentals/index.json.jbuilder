json.array!(@rentals) do |rental|
  json.extract! rental, :id, :productname, :productprice, :productquantity, :productimage
  json.url rental_url(rental, format: :json)
end
