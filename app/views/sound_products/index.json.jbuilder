json.array!(@sound_products) do |sound_product|
  json.extract! sound_product, :id, :maker, :name, :quantity, :price, :type
  json.url sound_product_url(sound_product, format: :json)
end
