json.array!(@movingfixtures) do |movingfixture|
  json.extract! movingfixture, :id, :fixturemaker, :fixturemodel, :fixturename, :led, :fixturewattage, :fixturequantity, :productimage
  json.url movingfixture_url(movingfixture, format: :json)
end
