json.array!(@wiredmics) do |wiredmic|
  json.extract! wiredmic, :id, :type, :quantity, :productimage
  json.url wiredmic_url(wiredmic, format: :json)
end
