json.array!(@suppliers) do |supplier|
  json.extract! supplier, :id, :companyname, :repname, :repphone, :repemail, :repaddress, :repcity, :repstate
  json.url supplier_url(supplier, format: :json)
end
