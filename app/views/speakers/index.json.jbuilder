json.array!(@speakers) do |speaker|
  json.extract! speaker, :id, :speakerbrand, :speakermodel, :speakertype, :speakerquantity
  json.url speaker_url(speaker, format: :json)
end
