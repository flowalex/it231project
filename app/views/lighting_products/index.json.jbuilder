json.array!(@lighting_products) do |lighting_product|
  json.extract! lighting_product, :id, :maker, :name, :quantity, :price, :type
  json.url lighting_product_url(lighting_product, format: :json)
end
