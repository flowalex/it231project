json.array!(@soundcables) do |soundcable|
  json.extract! soundcable, :id, :cabletype, :cablelenght, :cablequantity, :balanced
  json.url soundcable_url(soundcable, format: :json)
end
