json.array!(@used_sales) do |used_sale|
  json.extract! used_sale, :id, :productname, :productprice, :productquantity, :productimage
  json.url used_sale_url(used_sale, format: :json)
end
