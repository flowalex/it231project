json.array!(@lightingcables) do |lightingcable|
  json.extract! lightingcable, :id, :cabletype, :dmxpin, :cablequantityinteger, :cablelength
  json.url lightingcable_url(lightingcable, format: :json)
end
