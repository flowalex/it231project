json.array!(@wirelessmics) do |wirelessmic|
  json.extract! wirelessmic, :id, :type, :quantity, :productimage
  json.url wirelessmic_url(wirelessmic, format: :json)
end
