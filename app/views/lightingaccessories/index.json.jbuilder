json.array!(@lightingaccessories) do |lightingaccessory|
  json.extract! lightingaccessory, :id, :accessorytype, :compatablefixtures, :accessoryquantity, :productimage
  json.url lightingaccessory_url(lightingaccessory, format: :json)
end
