json.array!(@conventionalfixtures) do |conventionalfixture|
  json.extract! conventionalfixture, :id, :fixturemaker, :fixturemodel, :fixturename, :fixturelampwattage, :fixturequantity, :productimage
  json.url conventionalfixture_url(conventionalfixture, format: :json)
end
