json.array!(@ledfixtures) do |ledfixture|
  json.extract! ledfixture, :id, :fixturemaker, :fixturemodel, :fixturename, :fixtureledcolorscheme, :fixturequantity, :productimage
  json.url ledfixture_url(ledfixture, format: :json)
end
