json.array!(@employees) do |employee|
  json.extract! employee, :id, :firstname, :lastname, :address, :city, :state, :zip, :employeeemail, :employeephone
  json.url employee_url(employee, format: :json)
end
