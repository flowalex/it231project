json.array!(@newsales) do |newsale|
  json.extract! newsale, :id, :productname, :productprice, :productquantity, :productimage, :productdescription
  json.url newsale_url(newsale, format: :json)
end
