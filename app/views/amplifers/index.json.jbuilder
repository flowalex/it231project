json.array!(@amplifers) do |amplifer|
  json.extract! amplifer, :id, :ampliferbrand, :amplifiermodel, :amplifierpower, :amplifierquantity, :productimage
  json.url amplifer_url(amplifer, format: :json)
end
