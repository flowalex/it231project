json.array!(@lightingconsoles) do |lightingconsole|
  json.extract! lightingconsole, :id, :consolemaker, :consolemodel, :universe, :externalscreens, :consolequantity, :productimage
  json.url lightingconsole_url(lightingconsole, format: :json)
end
